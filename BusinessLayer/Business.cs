﻿using DataAccessLayer;
using DataAccessLayer.ModelClass;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Business
    {
        DataLayer datalayer = new DataLayer();
      public Task <bool> B_CheckUserNumber(string phoneNumber)
        {
            return ( datalayer.CheckUserNumber(phoneNumber));
        }


        public Task <bool> B_InsertUserNumber(string phoneNumber)
        {
            return datalayer.InsertUserNumber(phoneNumber);
        }


        public Task <CustomerData> B_FetchGarageDetails(string phoneNumber)
        {
            return datalayer.FetchGarageDetails(phoneNumber);
        }

        public Task <bool> B_InsertGarageDetails(CustomerData customer)
        {
            return  datalayer.InsertGarageDetailsAsync (customer); 
        }

        public Task<List<GarageCustomer>> B_FetchGarageCustomer(int garageid)
        {
            return  datalayer.FetchGarageCustomer(garageid);
        }

        public Task<bool> B_InsertGarageCustomerData(GarageCustomer garagecustomer)
        {
            return datalayer.InsertGarageCustomerData(garagecustomer);
        }
    }
}
