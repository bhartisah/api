﻿using DataAccessLayer.ModelClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class DataLayer
    {
        
        public async Task <bool>  CheckUserNumber (string number)
        {
            int status = 0;

            string connectionstring = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionstring))
            {
                SqlCommand command = new SqlCommand("Sp_CheckNumber", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@number", number);

                command.Parameters.Add("@status", SqlDbType.Bit).Direction = ParameterDirection.Output;

               await connection.OpenAsync();

                SqlDataReader dataReader = await  command.ExecuteReaderAsync();
                status =  Convert.ToInt32(command.Parameters["@status"].Value);
                
            }

            if (status == 1)
                return true;

            return false;
        }

        public async Task <bool> InsertUserNumber(string number)
        {
            int status = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("Sp_CustomerPhone_Isert", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@phone", number);
                command.Parameters.Add("@status", SqlDbType.Bit).Direction = ParameterDirection.Output;
                
                await connection.OpenAsync();

               await command.ExecuteNonQueryAsync();

                status = Convert.ToInt32(command.Parameters["@status"].Value);

            }

            if (status == 1)
                return true;
            return false;

        }

        public async Task< CustomerData> FetchGarageDetails(string number)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            CustomerData customerdata = new CustomerData();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("Sp_GarageDetail_Fetch", connection);

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@number", number);

              await  connection.OpenAsync();
                SqlDataReader dataReader = await command.ExecuteReaderAsync();


                while(await dataReader.ReadAsync())
                {
                    PhoneData data = new PhoneData()
                    {
                        PhoneNumber = dataReader.GetString (4),
                        Pk_PhoneId = dataReader.GetInt32(5)
                     };

                    //customerdata.phoneData.Pk_PhoneId = dataReader.GetInt32(4);
                    customerdata.GarageId =  Convert.ToInt32(dataReader[1].ToString());
                    customerdata.GarageName = dataReader.GetString(0);
                    customerdata.GarageEmailId = dataReader[2].ToString();
                    customerdata.GarageLogo = dataReader[3].ToString();
                    customerdata.phoneData = data;
                    //customerdata.phoneData.PhoneNumber = dataReader[3].ToString();
                    //ustomerdata.phoneData.Pk_PhoneId = Convert.ToInt32(dataReader[4]);
                }
            }

            return customerdata;
        }

        public async Task<bool> InsertGarageDetailsAsync(CustomerData data)
        {
            int status = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("Sp_Insert_GarageDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@gname" , data.GarageName);
                command.Parameters.AddWithValue("@gemail" , data.GarageEmailId);
                    command.Parameters.AddWithValue("@glogo" , data.GarageLogo);
                    command.Parameters.AddWithValue("@gphoneId" , data.phoneData.PhoneNumber);

                command.Parameters.Add("@status", SqlDbType.Bit).Direction = ParameterDirection.Output;
              await  connection.OpenAsync();

               await command.ExecuteNonQueryAsync();
                status =  Convert.ToInt32(command.Parameters["@status"].Value);
            }

            if (status == 1)
                return true;
            return false;
        }

        public async Task<bool> InsertGarageCustomerData(GarageCustomer garageCustomer)
        {
            int status = 0;

            string connectionstring = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionstring))
            {
                SqlCommand command = new SqlCommand("Sp_Insert_GarageCustomerData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@garageId", garageCustomer.garageId);

                command.Parameters.AddWithValue("@GarageCustomerName", garageCustomer.CustomerName);

               // command.Parameters.AddWithValue("@garageId", garageCustomer.garageId);

                command.Parameters.AddWithValue("@garageCustomerEmail", garageCustomer.CustomerEmailId);

                command.Parameters.AddWithValue("@SheduleDate", garageCustomer.SheduleDate);

                command.Parameters.AddWithValue("@sheduleTime", garageCustomer.SheduleTime);

                command.Parameters.AddWithValue("@custNumber", garageCustomer.CustomerNumber);

                command.Parameters.AddWithValue("@Note", garageCustomer.Note);

                command.Parameters.AddWithValue("@ChesisNumber", garageCustomer.vechileDetails.ChesisNumber);

                command.Parameters.AddWithValue("@VahicleRegNumber", garageCustomer.vechileDetails.VechicleRegNumber);

                command.Parameters.AddWithValue("@Make", garageCustomer.vechileDetails.Make);

                command.Parameters.AddWithValue("@Model", garageCustomer.vechileDetails.Model);
                command.Parameters.AddWithValue("@PurchaseDate", garageCustomer.vechileDetails.Purchasedate);


                command.Parameters.AddWithValue("@ModelYear", garageCustomer.vechileDetails.ModelYear);

                command.Parameters.AddWithValue("@NoOfCylinder", garageCustomer.vechileDetails.NoOfCylinder);
                command.Parameters.AddWithValue("@EngineCapacity", garageCustomer.vechileDetails.EngineCapacity);
                command.Parameters.AddWithValue("@VahicleColour", garageCustomer.vechileDetails.VechileColour);


                command.Parameters.Add("@status", SqlDbType.Bit).Direction = ParameterDirection.Output;

               await  connection.OpenAsync();
                await command.ExecuteNonQueryAsync();

                status = Convert.ToInt32( command.Parameters["@status"].Value);
            }
            if (status == 1)
                return true;

            return false;
        }

        public async Task<List<GarageCustomer>> FetchGarageCustomer(int garageId)
        {
            List<GarageCustomer> ListOfGarageCustomer = new List<GarageCustomer>();
 
            
            string connectionstring = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionstring))
            {
                SqlCommand command = new SqlCommand("Sp_Fetch_GarageCustomerData" , connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@garageId", garageId);

               await connection.OpenAsync();
               SqlDataReader datareader =  await command.ExecuteReaderAsync();


                while(await datareader.ReadAsync())
                {
                    GarageCustomer garagecustomer = new GarageCustomer();
                    VechileDetails vechile = new VechileDetails();
                    if (!datareader[8].Equals(DBNull.Value))
                    {
                        vechile.CustomerId = Convert.ToInt32(datareader[8]);
                    }
                    if (!datareader[9].Equals(DBNull.Value))
                    {
                        vechile.ChesisNumber = (datareader[9].ToString());
                    }
                    if (!datareader[10].Equals(DBNull.Value))
                    {
                        vechile.VechicleRegNumber    = (datareader[10].ToString());
                    }

                    if (!datareader[11].Equals(DBNull.Value))
                    {
                        vechile.Make = (datareader[11].ToString());
                    }

                    if (!datareader[12].Equals(DBNull.Value))
                    {
                        vechile.Model = (datareader[12].ToString());
                    }

                    if (!datareader[13].Equals(DBNull.Value))
                    {
                        vechile.Purchasedate = Convert.ToDateTime (datareader[13]);
                    }

                   if(!datareader[14].Equals(DBNull.Value))
                    {
                        vechile.ModelYear = Convert.ToInt32 (datareader[14]);
                    }

                    if (!datareader[15].Equals(DBNull.Value))
                    {
                        vechile.NoOfCylinder = Convert.ToInt32(datareader[15]);
                    }

                    if (!datareader[16].Equals(DBNull.Value))
                    {
                        vechile.EngineCapacity = (datareader[16].ToString());
                    }

                    if (!datareader[17].Equals(DBNull.Value))
                    {
                        vechile.VechileColour = (datareader[17].ToString());
                    }
                    //CustomerId = (Int32)datareader[8],
                    //ChesisNumber = datareader[9].ToString(),
                    //VechicleRegNumber = datareader[10].ToString(),
                    //Make = datareader[11].ToString(),
                    // Model = datareader[12].ToString(),
                    //Purchasedate = Convert.ToDateTime(datareader[13]),
                    //ModelYear = Convert.ToInt32(datareader[14]),
                    //NoOfCylinder = Convert.ToInt32(datareader[15]),
                    //EngineCapacity = datareader[16].ToString(),
                      //  VechileColour = datareader[17].ToString()
                   // };

                    garagecustomer.CustomerName = datareader[0].ToString();
                    garagecustomer.CustomerEmailId = datareader[1].ToString();
                    garagecustomer.SheduleDate = Convert.ToDateTime(datareader[2]);
                    //TimeSpan time = (TimeSpan)datareader[3];

                    garagecustomer.SheduleTime = (TimeSpan)datareader[3];
                    garagecustomer.Note = datareader[4].ToString();
                    garagecustomer.CustomerNumber = datareader[7].ToString();
                    garagecustomer.vechileDetails = vechile;

                    ListOfGarageCustomer.Add(garagecustomer);
                }

            }



            return ListOfGarageCustomer;
        }
    }
}
