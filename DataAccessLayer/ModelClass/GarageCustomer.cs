﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ModelClass
{
    public class GarageCustomer
    {
        public int garageId { set; get; }
        public int garageCustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerEmailId { get; set; }
        public DateTime SheduleDate { get; set; }
        public TimeSpan SheduleTime { get; set; }
        public string Note { get; set; }
        public VechileDetails vechileDetails { get; set; }
    }
}
