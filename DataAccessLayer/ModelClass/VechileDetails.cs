﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ModelClass
{
  public  class VechileDetails
    {
        public int VechileId { get; set; }
        public int CustomerId { get; set; }
        public string ChesisNumber { get; set; }
        public string VechicleRegNumber { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public DateTime Purchasedate { get; set; }
        public int  ModelYear { get; set; }
        public int NoOfCylinder { get; set; }
        public  string EngineCapacity { get; set; }
        public string VechileColour { get; set; }

    }
}
