﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ModelClass
{
  public  class PhoneData
    {
        public int Pk_PhoneId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
