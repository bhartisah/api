﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.ModelClass
{
   public class CustomerData
    {
        public int GarageId { get; set; }
        public string GarageName { get; set; }
        public string GarageEmailId { get; set; }
        public string GarageLogo { get; set; }
        public PhoneData phoneData { get; set; }
    }
}
