﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApplication3.LogDetails;
using DataAccessLayer.ModelClass;
using BusinessLayer;
using System.Collections.Generic;

namespace WebApplication3.Controllers
{
    public class GarageCustomerController : ApiController
    {
        BusinessLayer.Business businesslayer = new Business();

        [HttpPost]
        public async Task<HttpResponseMessage> InsertGarageCustomerDetails(GarageCustomer garageCustomer)
        {
            LogWritter writter = new LogWritter();
            try
            {
                bool status = await businesslayer.B_InsertGarageCustomerData(garageCustomer);
                if (status)
                {
                    writter.LogDetails("Api Controller GarageCustomer InsertGarageCustomerDetails Method", "Sucess");
                    return Request.CreateResponse(HttpStatusCode.Found, "The customer data has been inserted");
                }
                else
                {
                    writter.LogDetails("Api Controller GarageCustomer InsertGarageCustomerDetails Method", "Success");
                    return Request.CreateResponse(HttpStatusCode.NotFound, "The customer data has not been inserted ");
                }
            }


            catch (Exception exception)
            {
                writter.LogDetails("Api Controller GarageCustomer InsertGarageCustomerDetails Method", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError , "Plz Check");
            }
        }

        [HttpGet]

        public async Task<HttpResponseMessage> FetchGarageCustomerDetail (int garageid)
        {
            LogWritter writter = new LogWritter();
            List< GarageCustomer>  ListOfgaragecustomer = new List<GarageCustomer>();
            try
            {
                ListOfgaragecustomer = await businesslayer.B_FetchGarageCustomer(garageid);
                
                    writter.LogDetails("Api Controller GarageCustomer FetchGarageCustomerDetail Method", "Sucess");
                    return Request.CreateResponse(HttpStatusCode.Found, ListOfgaragecustomer);
                
                
            }


            catch (Exception exception)
            {
                writter.LogDetails("Api Controller GarageCustomer FetchGarageCustomerDetail Method", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError , "Plz check");
            }
        }
    }
}
