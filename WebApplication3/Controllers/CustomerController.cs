﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using WebApplication3.Authenticaation;
using WebApplication3.LogDetails;
using DataAccessLayer.ModelClass;
using System.Threading.Tasks;

namespace WebApplication3.Controllers
{
    //[BasicAuthentication]
    public class CustomerController : ApiController
    {
        BusinessLayer.Business businesslayer = new Business();

        [BasicAuthentication]
        [HttpGet]
        public async Task <HttpResponseMessage> CheckNumber(string number1)
        {
            LogWritter writter = new LogWritter();
            try
            {
                 bool status = await businesslayer.B_CheckUserNumber(number1);
                if (status)
                {
                    writter.LogDetails("Api Controller Check Number Method", "Sucess");
                    return  Request.CreateResponse(HttpStatusCode.Found, "The phone number is exist");
                }
                else
                {
                    writter.LogDetails("Api Controller Customer Check Number Method", "Success");
                    return Request.CreateResponse(HttpStatusCode.NotFound, "The Phone Number is not present");
                }
            }


            catch (Exception exception)
            {
                writter.LogDetails("Api Controller Customer Check Number Method ", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }





        [HttpPost]
        [BasicAuthentication]

        public async Task<HttpResponseMessage> InsertUserNumber(string number)
        {
            LogWritter writter = new LogWritter();

            try
            {
                bool status = await businesslayer.B_InsertUserNumber(number);
                if (status)
                {
                    writter.LogDetails("Api Controller Customer InsertUserNumber method ", "Sucess");
                    return Request.CreateResponse(HttpStatusCode.Created, "Phone Number Inserted");
                }
                else
                {
                    writter.LogDetails("Api Controller Customer InsertUserNumber method ", "Sucess");
                    return Request.CreateResponse(HttpStatusCode.Forbidden , "Phone Number Not Inserted");
                }
            }
            catch (Exception exception)
            {
                writter.LogDetails("Api Controller Customer InsertUserNumber method ", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, " API Issues .. Plz Report");
            }
        }

        [BasicAuthentication]
        [HttpGet]
        public async Task<HttpResponseMessage> FetchGarageDetails(String number)
        {
            CustomerData customerData = new CustomerData();
            LogWritter writter = new LogWritter();
            try
            {
                var garageDetails = await businesslayer.B_FetchGarageDetails(number);
                writter.LogDetails("Api Controller Customer FetchGarageDetails method ", "sucess");
                return Request.CreateResponse(HttpStatusCode.Found, garageDetails);

            }
            catch (Exception exception)
            {

                writter.LogDetails("Api Controller Customer FetchGarageDetails method ", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "plz check");
            }

        }


        [HttpPost]
        public async Task<HttpResponseMessage> InsertGarageDetails(CustomerData customer)
        {
            LogWritter writter = new LogWritter();

            try
            {
                bool status = await businesslayer.B_InsertGarageDetails(customer);
                if (status)
                {
                    writter.LogDetails("Api Controller Customer InsertGarageDetails method ", "Sucess");
                    return Request.CreateResponse(HttpStatusCode.Created, "Garage Details inserted");
                }
                else
                {
                    writter.LogDetails("Api Controller Customer InsertGarageDetails method ", "Failure");
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "Garage Details inserted");
                }
            }
            catch (Exception exception)
            {
                writter.LogDetails("Api Controller Customer InsertGarageDetails method ", exception.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, " API Issues .. Plz Report");
            }
        }

        [HttpGet]
        public string test()
        {
            return "testPassed";
        }


       
    }
}
