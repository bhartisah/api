﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication3.LogDetails
{
    public class LogWritter
    {
        public void LogDetails(string LayerName, string status)
        {
            var path = @"C:\Users\BHARTI SAH\Desktop\MVC Project\WebApplication3\WebApplication3\LogDetails\logfile";
            if (File.Exists(path))
            {
                File.AppendAllText(path, "LayerName = " + LayerName + " Status =  " + status + " Time = " +
                                DateTime.Today.ToString() +   DateTime.Now.ToString() + Environment.NewLine);
                
                File.AppendAllText(path, "**************************************************************************" + Environment.NewLine);
            }

            else
            {
                File.WriteAllText(path, "LayerName = " + LayerName + "Status = " + status + "Time = " +
                                 DateTime.Today.ToString() +   DateTime.Now.ToShortTimeString() + Environment.NewLine);
                File.AppendAllText(path, "**************************************************************************" + Environment.NewLine ) ;

            }
        }
    }
}